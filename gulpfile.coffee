###
	install npm install --save-dev gulp gulp-notify gulp-util gulp-concat gulp-stylus nib gulp-sourcemaps gulp-coffee gulp-uglify
###

gulp          = require 'gulp'
notify        = require 'gulp-notify' #Уведомления
gutil         = require 'gulp-util'   
#both
concat        = require 'gulp-concat'
#css
stylus        = require 'gulp-stylus'
nib           = require 'nib'
sourcemaps    = require 'gulp-sourcemaps'
#js
coffee        = require 'gulp-coffee'
uglify        = require 'gulp-uglify'

WATCH_STYLUS       = '_stylus/**/_*.styl'
WATCH_COFFEE       = '_coffee/*.coffee'

FILES_STYLUS       = '_stylus/[^\_]*.styl'
FILES_COFFEE       = '_coffee/*.coffee'

FILE_JS_NAME       = 'app.js'

PATH_JS_RELEASE    = 'release/'
PATH_JS_DEV        = 'js/'

PATH_CSS_RELEASE   = 'release/'
PATH_CSS_DEV       = 'css/'


jobdone = (what) ->
  notify "#{what} done \n #{do Date.now}"

###
  CSS
###
gulp.task 'css-dev', ->
  gulp
    .src FILES_STYLUS
    .pipe do sourcemaps.init
    .pipe stylus {use: nib(), compress: false}
    .pipe do sourcemaps.write
    .pipe gulp.dest PATH_CSS_DEV
    .pipe jobdone 'CSS dev'
  return  

gulp.task 'css-release', ->
  gulp
    .src FILES_STYLUS
    .pipe stylus {use: nib(), compress: true}
    .pipe gulp.dest PATH_CSS_RELEASE
    .pipe jobdone 'CSS release'
  return  

###concat
  JS
###
gulp.task 'js-dev', ->
  gulp
    .src FILES_COFFEE
    .pipe concat FILE_JS_NAME
    .pipe coffee({bare: true}).on 'error', gutil.log
    .pipe gulp.dest PATH_JS_DEV
    .pipe jobdone 'JS dev'
  return

#Компилировать кофи, с кложурированием, в разные файлы
gulp.task 'js-sep', ->
  gulp
    .src FILES_COFFEE
    .pipe coffee({bare: false}).on 'error', gutil.log
    .pipe gulp.dest PATH_JS_DEV
    .pipe jobdone 'JS sep'
  return


#Сорать все скомпиллированные js в один файл, аглифицировать, минифицировать
gulp.task 'js-release', ->
  gulp  
    .src FILES_COFFEE
    .pipe concat FILE_JS_NAME
    .pipe coffee({bare: true}).on 'error', gutil.log
    .pipe do uglify
    .pipe gulp.dest PATH_JS_RELEASE
    .pipe jobdone 'JS Release'
  return
  

###
 TASKS
###
gulp.task 'default', ['css-dev', 'js-dev']

gulp.task 'watch', ->
  gulp.watch WATCH_STYLUS, ['css-dev']
  gulp.watch WATCH_COFFEE, ['js-dev']
  return
  
gulp.task 'watch-sep', ->
  gulp.watch WATCH_STYLUS, ['css-dev']
  gulp.watch WATCH_COFFEE, ['js-sep']
  return
  
gulp.task 'release', ['css-release','js-release']

